/* global angular, Pace, App */

'use strict';

// Declare app level module which depends on views, and components
angular.module('app', ['ngResource', 'ngRoute', 'ui.bootstrap'])

        .config(function () {
            console.info('No configuration found for app');
        })


        .run([
            function () {
                console.info("AngularJS is running!");
            }
        ])


        .controller('AppController', ['$scope', '$filter', '$http', function ($scope, $filter, $http) {

                $scope.dismissModal = function () {
                    $('#amountModal').modal('hide');
                    delete $scope.amount;
                    delete $scope.selectedItem;
                };

                $scope.cart = [];
                $scope.items = [];
                $scope.totalPrice = 0;

                $http.get('/sales/listservices')
                        .success(function (response) {
                            $scope.items = response;
                        })
                        .error(function (response) {
                            window.alert("Failed loading items!");
                        });

                $scope.selectItem = function ($item, $model, $label, $event) {
                    $('#amountModal').modal('show');
                    $scope.selectedItem = $item;
//                    console.log($item);
                };

                $scope.addToCart = function (input) {
                    $scope.totalPrice += input;
                    var cartItem = angular.extend($scope.selectedItem, {"price": input});
                    $scope.cart.push(cartItem);
//                    console.log($scope.cart);
                    $scope.dismissModal();
                };

                $scope.delteItemFromCart = function (index) {
                    var item = $scope.cart[index];
                    $scope.cart.splice(index, 1);
                    $scope.totalPrice -= item.price;
                    delete $scope.tenderAmount;
                    delete $scope.tenderChange;
                };

                $scope.getTenderChanage = function (input) {
                    $scope.tenderChange = $filter('number')(input - $scope.totalPrice, 2);
                };

                $scope.checkout = function () {
                    var confirmed = window.confirm('Are you sure?');
                    if (confirmed) {
                        $('#checkoutButton').button('loading');
                        console.info("Checkout process has been initiated...");
                        var order = {
                            "items": $scope.cart,
                            "amount": $scope.tenderAmount,
                            "change": $scope.tenderChange
                        };
                        $http.post('/sales/checkout', order)
                                .success(function (response) {
                                    console.log(response);
                                    location.reload();
                                })
                                .error(function (response) {
                                    window.alert("Checkout failed!");
                                });
                    }
                };

            }]);

