<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function listUsers($limit, $offset,$search_term='')
	{
	
		$query = $this->db->select('SQL_CACHE *', FALSE)
			->from('aauth_users')
			->limit($limit, $offset)
			->order_by('date_created', 'DESC')
			->where('banned', 0);
       if($search_term!='')
			{
				$this->db->where("(`username` LIKE '%$search_term%')");
			}
		$rows['rows'] = $query->get()->result_array();
		
		$query = $this->db->select('COUNT(*)  as count', FALSE)

			->from('aauth_users')
			->where('banned', 0);
			if($search_term!='')
			{
				$this->db->where("(`username` LIKE '%$search_term%')");
			}
			
		$tmp = $query->get()->result();
		
		$rows['num_rows'] = $tmp[0]->count;
		
		return $rows;
	}

	public function getUser($id)
	{
		if($id)
  	{
  		$this->db->from('customers');
			$this->db->where('id',$id);
	    $query = $this->db->get();
	    $row = $query->row_array();
	    return $row;
  	}
  	else
  	{
  		return FALSE;
  	}
	}

	public function deleteUserGroups($userid)
	{
		if($userid)
		{
			$this->db->where('user_id', $userid);
			return $this->db->delete('aauth_user_to_group');
		}
		return FALSE;
	}
}

/* End of file Users_model.php */
/* Location: ./application/models/Users_model.php */