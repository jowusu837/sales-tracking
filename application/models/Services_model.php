<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model {

    public $variable;

    public function __construct() {
        parent::__construct();
    }

    public function saveService($data) {
        if ($data) {
            if ($this->db->insert('services', $data)) {
                return $this->db->insert_id();
            }
            return true;
        }
        return false;
    }

    public function getService($id) {
        if ($id) {
            $this->db->from('services');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $row = $query->row_array();
            return $row;
        } else {
            return FALSE;
        }
    }

    public function updateService($id, $data) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->update('services', $data);
        } else {
            return FALSE;
        }
    }

    public function getServices() {
        $this->db->from('services');
        $this->db->where('status', 'active');
        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }
    
    public function getInventoryItems() {
        $this->db->from('services')
                ->where('status', 'active')
                ->where('inventory_part', 1);
        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    public function list_services($limit, $offset, $search_term = '') {

        $query = $this->db->select('SQL_CACHE services.*, categories.name as category_name', FALSE)
                ->from('services')
                ->join('categories', 'categories.id = services.category_id', 'left')
                ->limit($limit, $offset)
                ->order_by('service_name', 'ASC')
                ->where('status', 'active');
        if ($search_term != '') {
            $this->db->where("(`service_name` LIKE '%$search_term%')");
        }
        $rows['rows'] = $query->get()->result_array();

        $query = $this->db->select('COUNT(*)  as count', FALSE)
                ->from('services')
                ->where('status', 'active');
        if ($search_term != '') {
            $this->db->where("(`service_name` LIKE '%$search_term%')");
        }

        $tmp = $query->get()->result();

        $rows['num_rows'] = $tmp[0]->count;

        return $rows;
    }

    public function totalNumServices() {
        $query = $this->db->select("COUNT(*) AS total", FALSE);
        $this->db->from('services');
        $query = $this->db->get();
        $row = $query->row_array();
        return $row['total'];
    }

    public function getServicesAjax() {
        $this->db->select('id, service_name as label, service_price')
                ->from('services')
                ->where('status', 'active')
                ->where('not_for_sale', 0)
                ->order_by('service_name', 'ASC');
        $query = $this->db->get();
        return $query->result_array();
    }

}

/* End of file Services_model.php */
/* Location: ./application/models/Services_model.php */