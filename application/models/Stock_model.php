<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Stock_model extends CI_Model {

    public $variable;

    public function __construct() {
        parent::__construct();
    }

    public function save($data) {
        if ($data) {
            if ($this->db->insert('stock', $data)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
        return false;
    }

    public function saveUsed($data) {
        if ($data) {
            if ($this->db->insert('used_items', $data)) {
                return $this->db->insert_id();
            } else {
                return false;
            }
        }
        return false;
    }

    public function update($id, $data) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->update('stock', $data);
        } else {
            return false;
        }
    }

    public function updateUsed($id, $data) {
        if ($id) {
            $this->db->where('id', $id);
            return $this->db->update('used_items', $data);
        } else {
            return false;
        }
    }

    public function saveOrUpdate($id, $data) {
        $this->db->from('stock');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $row = $query->row_array();
        if ($row) {
            return $this->update($id, $data);
        } else {
            return $this->save($data);
        }
    }

    public function saveOrUpdateUsed($id, $data) {
        $this->db->from('used_items');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $row = $query->row_array();
        if ($row) {
            return $this->updateUsed($id, $data);
        } else {
            return $this->saveUsed($data);
        }
    }

    public function get($id) {
        if ($id) {
            $this->db->from('stock');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $row = $query->row_array();
            return $row;
        } else {
            return FALSE;
        }
    }
    
    public function getUsed($id) {
        if ($id) {
            $this->db->from('used_items');
            $this->db->where('id', $id);
            $query = $this->db->get();
            $row = $query->row_array();
            return $row;
        } else {
            return FALSE;
        }
    }

    public function listItems($limit, $offset, $search_term = '') {

        $query = $this->db->select('SQL_CACHE services.id as id,services.service_name as item_name, services.service_price as unit_price, services.not_for_sale as not_for_sale,categories.name as category_name,(SUM(stock.quantity)-count(sale_items.id)-SUM(used_items.quantity)) as qty_in_stock', FALSE)
                ->from('services')
                ->join('stock', 'stock.item_id = services.id', 'left')
                ->join('used_items', 'used_items.item_id = services.id', 'left')
                ->join('sale_items', 'sale_items.service_id = services.id', 'left')
                ->join('categories', 'services.category_id = categories.id', 'left')
                ->limit($limit, $offset)
                ->where('services.inventory_part', '1')
                ->group_by('services.id');
        if ($search_term != '') {
            $this->db->where("(`service_name` LIKE '%$search_term%')");
        }
        $rows['rows'] = $query->get()->result_array();
        $rows['num_rows'] = $query->affected_rows();

        return $rows;
    }

}

/* End of file Stock_model.php */
/* Location: ./application/models/Stock_model.php */
