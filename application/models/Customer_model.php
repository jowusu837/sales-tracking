<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer_model extends CI_Model {

	public $variable;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function getCustomers()
	{
		$this->db->from('customers');
		$this->db->where('status','active');
		$query = $this->db->get();
	  $rows = $query->result_array();
	  return $rows;
	}

	public function saveCustomer($data)
	{
		if($data)
		{
			if($this->db->insert('customers', $data))
			{
				return $this->db->insert_id();
			}
			return true;
		}
		return false;
	}

	public function getCustomer($id)
	{
		if($id)
	  	{
	  		$this->db->from('customers');
				$this->db->where('id',$id);
		    $query = $this->db->get();
		    $row = $query->row_array();
		    return $row;
	  	}
	  	else
	  	{
	  		return FALSE;
	  	}
	}

	public function updateCustomer($id,$data)
	{
		if($id)
	  	{
	  		$this->db->where('id', $id);
				return $this->db->update('customers', $data);
	  	}
	  	else
	  	{
	  		return FALSE;
	  	}
	}

	public function list_customers($limit, $offset,$search_term='')
	{
	
		$query = $this->db->select('SQL_CACHE *', FALSE)
			->from('customers')
			->limit($limit, $offset)
			->order_by('created_at', 'ASC')
			->where('status', 'active');
       if($search_term!='')
			{
				$this->db->where("(`first_name` LIKE '%$search_term%') OR (`last_name` LIKE '%$search_term%')");
			}
		$rows['rows'] = $query->get()->result_array();
		
		$query = $this->db->select('COUNT(*)  as count', FALSE)

			->from('customers')
			->where('status', 'active');
			if($search_term!='')
			{
				$this->db->where("(`first_name` LIKE '%$search_term%') OR (`last_name` LIKE '%$search_term%')");
			}
			
		$tmp = $query->get()->result();
		
		$rows['num_rows'] = $tmp[0]->count;
		
		return $rows;
	}

	public function totalNumCustomers()
	{
		$query = $this->db->select("COUNT(*) AS total", FALSE);
		$this->db->from('customers');
		$query = $this->db->get();
	  $row = $query->row_array();
	  return $row['total'];
	}
}

/* End of file Customer_model.php */
/* Location: ./application/models/Customer_model.php */