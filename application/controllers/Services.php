<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

    private $data = array();
    private $pagelist;
    private $limit = 10;
    private $offset = 0;

    public function __construct() {
        parent::__construct();
        $this->data['msg'] = '';
        $this->load->library("Aauth");
        $this->load->library('form_validation');
        $this->load->model('Services_model');
        $this->load->model('Categories_model');
        $this->load->helper('form');
        $this->load->helper('services');
        $this->data['serviceInfo'] = array('price' => '', 'type' => '', 'name' => '');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
        if ($this->aauth->is_loggedin() === false) {
            redirect('login', 'location');
        }

        if (!$this->aauth->is_admin()) {
            redirect('sales', 'location');
        }

        $this->data['userinfo'] = $this->session->userdata('userinfo');
        $this->data['typedata'] = array(NULL => 'Uncategorized');
        $categories = $this->Categories_model->getAll();
        foreach ($categories as $category) {
            $this->data['typedata'][$category['id']] = $category['name'];
        }
        $this->data['serviceInfo']['service_name'] = '';
        $this->data['serviceInfo']['service_price'] = '';
        $this->data['serviceInfo']['service_type'] = '';
        $this->data['serviceInfo']['inventory_part'] = '';
        $this->data['serviceInfo']['category_id'] = NULL;
        $this->data['serviceInfo']['not_for_sale'] = 0;
        $this->data['pagination'] = $this->data['search_term'] = '';
        $this->form_validation->set_rules('name', 'Services name', 'required');
        if(!$this->input->post('not_for_sale')){
            $this->form_validation->set_rules('price', 'Price', 'required');
        }
    }

    public function index() {
        if ($this->input->get('page')) {
            $this->page = $this->input->get('page');
            $this->offset = ($this->page == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
        }
        $results = $this->Services_model->list_services($this->limit, $this->offset, $this->data['search_term']);
        $this->pagelist = $results['rows'];
        $this->data['pagelist'] = listTable($this->pagelist);
        $this->data['num_results'] = $results['num_rows'];
        $this->load->library('pagination');
        $this->pagnconfig['base_url'] = base_url('services');
        $this->pagnconfig['total_rows'] = $this->data['num_results'];
        $this->pagnconfig['per_page'] = $this->limit;
        $choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
        $this->pagination->initialize($this->pagnconfig);
        $this->data['pagination'] = $this->pagination->create_links();

        $this->template->default_tmpl('services', $this->data);
        $this->config->load('pagination');
    }

    public function add() {
        $userinfo = $this->data['userinfo'];

        if ($this->form_validation->run()) {
            $name = strtoupper($this->input->post('name'));
            $price = $this->input->post('price');
            $categoryId = $this->input->post('category_id');
            $inventoryPart = (bool) $this->input->post('inventory_part');
            $notForSale = (bool) $this->input->post('not_for_sale');
            $inputdata = array(
                'service_name' => $name,
                'service_price' => $price,
                'created_at' => gmdate('Y-m-d H:i:s'),
                'created_by' => $userinfo->id,
                'category_id' => $categoryId,
                'inventory_part' => $inventoryPart,
                'not_for_sale' => $notForSale
            );
            $insert = $this->Services_model->saveService($inputdata);
            if ($insert) {
                $this->session->set_flashdata('msg', notification("Menu item has been added successfully!", 'success'));
                redirect('services/edit/' . $insert, 'location');
            }
        }
        $this->template->default_tmpl('services-entry', $this->data);
    }

    public function edit($id = 0) {
        if ($this->form_validation->run()) {
            $name = strtoupper($this->input->post('name'));
            $price = $this->input->post('price');
            $categoryId = $this->input->post('category_id');
            $inventoryPart = (bool) $this->input->post('inventory_part');
            $notForSale = (bool) $this->input->post('not_for_sale');
            $inputdata = array('service_name' => $name, 'service_price' => $price, 'category_id' => $categoryId, 'inventory_part' => $inventoryPart, 'not_for_sale' => $notForSale);
            if ($this->Services_model->updateService($id, $inputdata)) {
                $this->session->set_flashdata('msg', notification("Service updated successfully!", 'success'));
                redirect('services/edit/' . $id, 'location');
            }
        }
        $serviceInfo = $this->Services_model->getService($id);
        $this->data['serviceInfo'] = $serviceInfo;
        $this->template->default_tmpl('services-entry', $this->data);
    }

    public function delete($id = 0) {
        $this->Services_model->updateService($id, array('status' => 'deleted'));
        $this->session->set_flashdata('msg', notification("Service deleted successfully!", 'success'));
        redirect('services', 'location');
    }

}

/* End of file Services.php */
/* Location: ./application/controllers/Services.php */