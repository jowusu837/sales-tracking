<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_person extends CI_Controller {
	private $data=array();
	private $pagelist;
	private $limit=15;
	private $offset=0;
	public function __construct()
	{
		parent::__construct();
		$this->data['msg']='';
		$this->load->library("Aauth");
		$this->load->library('form_validation');
		$this->load->model('Serviceperson_model');
		$this->load->helper('form');
		$this->load->helper('services');
		$this->data['serviceInfo']=array('price'=>'','type'=>'','name'=>'');
		$this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
		if($this->aauth->is_loggedin()===false)
		{
			redirect('login','location');
		}
		$this->data['userinfo']=$this->session->userdata('userinfo');
		$this->data['name']='';
		$this->data['phone']='';
		$this->data['started_work']='';
		$this->data['terminated_work']='';
		$this->data['address']='';
		$this->data['status']='';
		$this->data['statusdata']=array('active'=>'Active','inactive'=>'Inactive');
		$this->data['pagination'] =$this->data['search_term']='';
		$this->form_validation->set_rules('name', $this->config->item('app_service_person').' Name', 'required');
	}

	public function index()
	{
		if($this->input->get('page'))
		{
			$this->page=$this->input->get('page');
			$this->offset = ($this->page  == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
		}
		$results = $this->Serviceperson_model->list_persons($this->limit, $this->offset,$this->data['search_term']);
		$this->pagelist= $results['rows'];
		$this->data['pagelist'] = servicePersonlistTable($this->pagelist);
		$this->data['num_results'] = $results['num_rows'];
		$this->load->library('pagination');
		$this->pagnconfig['base_url'] = base_url('services');
		$this->pagnconfig['total_rows'] = $this->data['num_results'];
		$this->pagnconfig['per_page'] = $this->limit;
		$choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
		$this->pagination->initialize($this->pagnconfig);
		$this->data['pagination'] = $this->pagination->create_links();

		$this->template->default_tmpl('service-person',$this->data);
		$this->config->load('pagination');
	}

	public function add()
	{
		$userinfo=$this->data['userinfo'];
		
		if($this->form_validation->run())
		{
			$name=$this->input->post('name');
			$phone=$this->input->post('phone');
			$started_work=$this->input->post('started_work');
			$terminated_work=$this->input->post('terminated_work');
			$address=$this->input->post('address');
			$status=$this->input->post('status');
			$started_work=($started_work)? gmdate('Y-m-d',strtotime($started_work)):'';
			$terminated_work=($terminated_work)? gmdate('Y-m-d',strtotime($terminated_work)):'';
			$created_at=gmdate('Y-m-d H:i:s');
			$created_by=$userinfo->id;
			$inputdata=array('name'=>$name
											,'phone'=>$phone
											,'started_work'=>$started_work
											,'terminated_work'=>$terminated_work
											,'address'=>$address
											,'status'=>$status
											,'created_at'=>$created_at
											,'created_by'=>$created_by
											);
			$inserted=$this->Serviceperson_model->saveServicePerson($inputdata);
			if($inserted)
			{
				$this->session->set_flashdata('msg', notification("Service added successfully!",'success'));
				redirect('service-person/edit/'.$inserted,'location');
			}
		}
		$this->template->default_tmpl('services-person-entry',$this->data);
	}

	public function edit($id=0)
	{
		if($this->form_validation->run())
		{
			$name=$this->input->post('name');
			$phone=$this->input->post('phone');
			$started_work=$this->input->post('started_work');
			$terminated_work=$this->input->post('terminated_work');
			$address=$this->input->post('address');
			$status=$this->input->post('status');
			$started_work=($started_work)? gmdate('Y-m-d',strtotime($started_work)):'';
			$terminated_work=($terminated_work)? gmdate('Y-m-d',strtotime($terminated_work)):'';
			$inputdata=array('name'=>$name
											,'phone'=>$phone
											,'started_work'=>$started_work
											,'terminated_work'=>$terminated_work
											,'address'=>$address
											,'status'=>$status
											);
			$this->Serviceperson_model->updateServicePerson($id,$inputdata);
			$this->session->set_flashdata('msg', notification($this->config->item('app_service_person'). "'s information updated successfully!",'success'));
			redirect('service-person/edit/'.$id,'location');
		}
		$servicePersonInfo=$this->Serviceperson_model->getServicePerson($id);
		$this->data['name']=$servicePersonInfo['name'];
		$this->data['phone']=$servicePersonInfo['phone'];
		$this->data['started_work']=$servicePersonInfo['started_work'];
		$this->data['terminated_work']=$servicePersonInfo['terminated_work'];
		$this->data['address']=$servicePersonInfo['address'];
		$this->data['status']=$servicePersonInfo['status'];
		$this->template->default_tmpl('services-person-entry',$this->data);
	}

	public function delete($id=0)
	{
		$this->Serviceperson_model->updateServicePerson($id,array('status'=>'deleted'));
		$this->session->set_flashdata('msg', notification($this->config->item('app_service_person')." deleted successfully!",'success'));
		redirect('/service-person/','location');
	}

}

/* End of file Services.php */
/* Location: ./application/controllers/Service_person.php */