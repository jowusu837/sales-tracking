<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_report extends CI_Controller {

    private $data;

    public function __construct() {
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->load->library("Aauth");
        $this->load->library('form_validation');
        $this->load->model('Sales_model');
        $this->load->model('Services_model');
        $this->load->model('Customer_model');
        $this->load->model('Serviceperson_model');
        $this->load->model('Categories_model');
        $this->load->helper('sales');
        if ($this->aauth->is_loggedin() === false) {
            redirect('login', 'location');
        }
        if (!$this->aauth->is_admin()) {
            redirect('sales', 'location');
        }
        $this->data['userinfo'] = $this->session->userdata('userinfo');
        $this->data['reportlist'] = '';
        $this->data['categories'] = array();
        $this->data['selectedCategory'] = '';
        $this->data['category_id'] = '';
        $this->data['servicedata'] = '';
        $this->data['serviceSelected'] = $this->data['servicePersonData'] = $this->data['customerdata'] = '';
        $this->data['customerSelected'] = '';
        $this->data['servicepersonSelected'] = '';
    }

    public function index() {
        $categoryItems = array();
        $categories = $this->Categories_model->getAll();
        $categoryItems[''] = 'All Categories';
        foreach ($categories as $item) {
            $categoryItems[$item['id']] = $item['name'];
        }
        $this->data['categories'] = $categoryItems;
        
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getServices();
        $serviceDataItems[''] = 'All Services';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;

        $servicePersonDataItems = array();
        $servicePersonData = $this->Serviceperson_model->getServicePersons();
        $servicePersonDataItems[''] = 'All ' . $this->config->item('app_service_person') . 's';
        foreach ($servicePersonData as $item) {
            $servicePersonDataItems[$item['id']] = $item['name'];
        }
        $this->data['servicePersonData'] = $servicePersonDataItems;

        $customerData = $this->Customer_model->getCustomers();
        $customerDataItems[''] = 'All Customers';
        foreach ($customerData as $item) {
            $customerDataItems[$item['id']] = $item['first_name'] . ' ' . $item['last_name'];
        }
        $this->data['customerdata'] = $customerDataItems;

        $end_date = gmdate('Y-m-d');
        $start_date = gmdate('Y-m-d', strtotime('today - 30 days'));
        if ($this->input->get('salesdaterange')) {
            $salesdaterange = $this->input->get('salesdaterange');
            $salesdaterange_arr = ($salesdaterange) ? explode(' - ', $salesdaterange) : array();
            $start_date = gmdate('Y-m-d', strtotime($salesdaterange_arr[0]));
            $end_date = gmdate('Y-m-d', strtotime($salesdaterange_arr[1]));

            $this->data['serviceSelected'] = $this->input->get('service');
            $this->data['customerSelected'] = $this->input->get('customer');
            $this->data['servicepersonSelected'] = $this->input->get('serviceperson');
            $this->data['selectedCategory'] = $this->input->get('category_id');
        }
        $this->data['start_date'] = $start_date;
        $this->data['end_date'] = $end_date;
        $salesItems = $this->Sales_model->Sales_report($start_date, $end_date, $this->data['selectedCategory'], $this->data['serviceSelected'], $this->data['customerSelected'], $this->data['servicepersonSelected']);
        $this->data['reportlist'] = ($salesItems) ? listSalesReportTable($salesItems) : '<div class="callout callout-info"><h4>No Results Found</h4></div>';
        $this->template->default_tmpl('sales-report', $this->data);
    }

}

/* End of file Sales_report.php */
/* Location: ./application/controllers/Sales_report.php */