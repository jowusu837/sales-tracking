<?php

defined('BASEPATH') OR exit('No direct script access allowed');
/*
  ALTER TABLE  `sale_items` CHANGE  `sale_type`  `sale_type` ENUM(  'cash',  'credit',  'administrative',  'mobilemoney' ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT  'cash';
 */

class Sales extends CI_Controller {

    private $data;

    public function __construct() {
        parent::__construct();
        $this->load->library("Aauth");
        $this->load->library('form_validation');
        if ($this->aauth->is_loggedin() === false) {
            redirect('login', 'location');
        }
        $this->data['userinfo'] = $this->session->userdata('userinfo');
        $this->data['serviceSelected'] = $this->data['servicePersonSelected'] = $this->data['customerSelected'] = '';
        $this->data['servicedata'] = array();
        $this->data['customerdata'] = array();
        $this->data['latestsaleslist'] = '';
        $this->data['receiptDisplay'] = '';
        $this->load->model('Services_model');
        $this->load->model('Customer_model');
        $this->load->model('Serviceperson_model');
        $this->load->model('Sales_model');
        $this->load->helper('sales');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
        $this->data['msg'] = '';
    }

    public function index() {
        $customerid = '';
        $this->data['latestsaleslist'] = listLatestSales($this->Sales_model->getLatestSales(8, $this->data['userinfo']->id));
        $this->form_validation->set_rules('service', 'Service', 'required');
        $this->form_validation->set_rules('tender', 'Tender', 'required');
        $this->form_validation->set_rules('mm_reference', 'Mobile Money Reference', 'callback_mm_reference_check');
        if ($this->form_validation->run()) {
            $serviceprice = $this->input->post('serviceprice');
            $serviceid = $this->input->post('service');
            $tender = $this->input->post('tender');
            $serviceperson = $this->input->post('serviceperson');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $email = $this->input->post('email');
            $gender = $this->input->post('gender');
            $phone = $this->input->post('phone');
            $address = $this->input->post('address');
            $customer_id = $this->input->post('customer_id');
            $mm_reference = $this->input->post('mm_reference');
            $payment_type = $this->input->post('payment_type');
            $created_by = $this->data['userinfo']->id;
            $created_at = gmdate('Y-m-d H:i:s');
            $sales_by = $this->data['userinfo']->id;
            $sales_at = gmdate('Y-m-d H:i:s');
            $customerDBData = array('first_name' => $first_name, 'last_name' => $last_name, 'email' => $email, 'phone' => $phone, 'gender' => $gender, 'address' => $address, 'created_by' => $created_by, 'created_at' => $created_at);

            $serviceData = $this->Services_model->getService($serviceid);
            $servicePriceDB = $serviceData['service_price'];
            if ($tender < $servicePriceDB && $tender != $servicePriceDB) {
                $this->data['msg'] = notification('Tender must be equal or greater than price', 'alert');
            } else {
                $change = $tender - $serviceprice;
                if (isset($customer_id)) {
                    $customerid = $customer_id;
                } elseif ($first_name != '') {
                    $customerid = $this->saveCustomerDetails($customerDBData);
                }
                $salesData = array('tender' => $tender, 'change_amount' => $change, 'service_person' => $serviceperson, 'sales_by' => $sales_by, 'sales_at' => $sales_at, 'sales_to' => $customerid);
                $salesid = $this->Sales_model->saveSales($salesData);
                if ($salesid) {
                    $saleItemData = array('sales_id' => $salesid, 'customer_id' => $customerid, 'sale_type' => $payment_type, 'amount' => $serviceprice, 'service_id' => $serviceid, 'reference' => $mm_reference);
                    $saleitemid = $this->Sales_model->saveSaleItem($saleItemData);
                    if ($saleitemid) {
                        $this->session->set_flashdata('msg', notification("Sale added successfully!", 'success'));
                        $this->session->set_flashdata('saleitemid', $saleitemid);

                        //redirect('sales','location');
                    }
                }
            }
        }
        $serviceDataItems = array();
        $serviceData = $this->Services_model->getServices();
        $serviceDataItems[''] = 'Select';
        foreach ($serviceData as $item) {
            $serviceDataItems[$item['id']] = $item['service_name'];
        }
        $this->data['servicedata'] = $serviceDataItems;

        $servicePersonDataItems = array();
        $servicePersonData = $this->Serviceperson_model->getServicePersons();
        $servicePersonDataItems[''] = 'Select';
        foreach ($servicePersonData as $item) {
            $servicePersonDataItems[$item['id']] = $item['name'];
        }
        $this->data['servicePersonData'] = $servicePersonDataItems;

        $customerData = $this->Customer_model->getCustomers();
        $customerDataItems[''] = 'Select';
        foreach ($customerData as $item) {
            $customerDataItems[$item['id']] = $item['first_name'] . ' ' . $item['last_name'] . ' -- ' . getTimeDifference($item['created_at']);
        }
        $this->data['customerdata'] = $customerDataItems;

        $this->template->default_tmpl('sales', $this->data);
    }

    public function servicedetails($service_id = 0) {
        $service_id = $this->input->get('service_id');
        $serviceData = $this->Services_model->getService($service_id);
        echo json_encode($serviceData);
    }

    public function saveCustomerDetails($data) {
        $customerid = '';
        if ($data) {
            $customerid = $this->Customer_model->saveCustomer($data);
        }
        return $customerid;
    }

    public function receipt($sales_id) {
        $salesItems = $this->Sales_model->Sales_receipt($sales_id);
        $this->data['receiptDisplay'] = print_receipt($sales_id, $salesItems);
        $this->template->default_plain('sales-receipt', $this->data);
    }

    public function mm_reference_check() {
        if ($this->input->post('payment_type') == 'mobilemoney' && ($this->input->post('mm_reference') == '')) {
            $this->form_validation->set_message('mm_reference_check', 'Provide reference');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function checkout() {
        header("Content-type: application/json");
        $data = json_decode(file_get_contents("php://input"), TRUE);
        $salesData = array(
            'tender' => $data['amount'],
            'change_amount' => $data['change'],
            'service_person' => '',
            'sales_by' => $this->data['userinfo']->id,
            'sales_at' => gmdate('Y-m-d H:i:s'),
            'sales_to' => ''
        );
        $salesid = $this->Sales_model->saveSales($salesData);

        foreach ($data['items'] as $item) {
            if ($salesid) {
                $saleItemData = array(
                    'sales_id' => $salesid,
                    'customer_id' => '',
                    'sale_type' => 'cash',
                    'amount' => $item['price'],
                    'service_id' => $item['id'],
                    'reference' => ''
                );

                $this->Sales_model->saveSaleItem($saleItemData);
            }
        }

        echo json_encode(array('message' => 'SUCCESS'));
    }
    
    public function listServices() {
        header("Content-type: application/json");
        $data = $this->Services_model->getServicesAjax();
        echo json_encode($data);
    }

}

/* End of file Sales.php */
/* Location: ./application/controllers/Sales.php */