<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library("Aauth");
		$this->load->helper('dashboard');
	}

	public function index()
	{
		$data=array();
		$data['userinfo']='';
		if ($this->aauth->is_loggedin())
		{
			$this->aauth->create_perm('access_sales_reports');
			$this->aauth->create_perm('access_users');
			$this->aauth->allow_group('Admin','access_sales_reports');
			$this->aauth->allow_group('Admin','access_users');
			$data['userinfo']=$this->session->userdata('userinfo');
			$this->template->default_tmpl('dashboard',$data);
		}
		else
		{
			redirect('login/','location');
		}
		if(!$this->aauth->is_admin())
    {
     redirect('sales','location');
    }
	}

	public function create_user()
	{
    $a = $this->aauth->create_user("lilmopat@gmail.com", "123456", "barber");
    if($a)
      echo "tmm   ";
    else
     echo "hyr  ";
    $this->aauth->print_errors();
  }

}

/* End of file Dashboard.php */
/* Location: ./application/controllers/Dashboard.php */