<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @property Categories_model $Categories_model
 */

class Categories extends CI_Controller {

    private $data = array();
    private $pagelist;
    private $limit = 10;
    private $offset = 0;

    public function __construct() {
        parent::__construct();
        $this->data['msg'] = '';
        $this->load->library("Aauth");
        $this->load->library('form_validation');
        $this->load->model('Categories_model');
        $this->load->helper('form');
        $this->load->helper('services');
        $this->form_validation->set_error_delimiters('<div class="alert alert-error alert-dismissible">', '</div>');
        if ($this->aauth->is_loggedin() === false) {
            redirect('login', 'location');
        }

        if (!$this->aauth->is_admin()) {
            redirect('sales', 'location');
        }

        $this->data['userinfo'] = $this->session->userdata('userinfo');
        $this->data['typedata'] = array();
        $categories = $this->Categories_model->getAll();
        foreach ($categories as $category) {
            $this->data['typedata'][$category['id']] = $category['name'];
        }
        $this->data['name'] = '';
        $this->data['description'] = '';
        $this->data['pagination'] = $this->data['search_term'] = '';
        $this->form_validation->set_rules('name', 'Category name', 'required');
    }

    public function index() {
        if ($this->input->get('page')) {
            $this->page = $this->input->get('page');
            $this->offset = ($this->page == 1) ? 0 : ($this->page * $this->limit) - $this->limit;
        }
        $results = $this->Categories_model->list_categories($this->limit, $this->offset, $this->data['search_term']);
        $this->pagelist = $results['rows'];
        $this->data['pagelist'] = $this->pagelist;
        $this->data['num_results'] = $results['num_rows'];
        $this->load->library('pagination');
        $this->pagnconfig['base_url'] = base_url('categories');
        $this->pagnconfig['total_rows'] = $this->data['num_results'];
        $this->pagnconfig['per_page'] = $this->limit;
        $choice = $this->pagnconfig["total_rows"] / $this->pagnconfig["per_page"];
        $this->pagination->initialize($this->pagnconfig);
        $this->data['pagination'] = $this->pagination->create_links();

        $this->template->default_tmpl('categories', $this->data);
        $this->config->load('pagination');
    }

    public function add() {
        if ($this->form_validation->run()) {
            $name = $this->input->post('name');
            $description = $this->input->post('description');
            $inputdata = array(
                'name' => $name,
                'description' => $description
            );
            $insert = $this->Categories_model->save($inputdata);
            if ($insert) {
                $this->session->set_flashdata('msg', notification("Category has been added successfully!", 'success'));
                redirect('categories/edit/' . $insert, 'location');
            }
        }
        $this->template->default_tmpl('categories-entry', $this->data);
    }

    public function edit($id = 0) {
        if ($this->form_validation->run()) {
            $name = $this->input->post('name');
            $description = $this->input->post('description');
            $inputdata = array('name' => $name, 'description' => $description);
            if ($this->Categories_model->updateCategory($id, $inputdata)) {
                $this->session->set_flashdata('msg', notification("Category has been updated successfully!", 'success'));
                redirect('categories/edit/' . $id, 'location');
            }
        }
        $category = $this->Categories_model->getCategory($id);
        $this->data['name'] = $category['name'];
        $this->data['description'] = $category['description'];
        $this->template->default_tmpl('categories-entry', $this->data);
    }

    public function delete($id = 0) {
        $this->Categories_model->updateCategory($id, array('status' => 'deleted'));
        $this->session->set_flashdata('msg', notification("Category deleted successfully!", 'success'));
        redirect('categories', 'location');
    }

}