  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Customers
        <?php $page_mode=($this->uri->segment(2)!='edit')? 'new':'edit';?>
        <small><?php echo ucwords($page_mode);?></small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
      <div class="col-xs-12">
       <?php echo $this->session->flashdata('msg');?>
      <form method="post">
         <div class="box">
                    <div class="box-header with-border">
                      <h3 class="box-title"><?php echo ucwords($page_mode);?> Customer</h3>
                    </div>
                    <div class="box-body">
                  <fieldset>
                  <div class="row">
                     <div class="col-xs-6">
                      <div class="form-group">
                        <div class="infobox"></div>
                        <label for="inputname">First Name</label>
                        <input class="form-control input" tabindex="2" id="first_name" name="first_name" type="text" placeholder="Customer First Name" value="<?php echo set_value('first_name',$first_name); ?>">
                        <?php echo form_error('first_name'); ?>
                      </div>
                      </div>

                      <div class="col-xs-6">
                       <div class="form-group">
                        <div class="infobox"></div>
                        <label for="inputname">Last Name</label>
                        <input class="form-control input" tabindex="3" id="last_name" name="last_name" type="text" placeholder="Customer Last Name" value="<?php echo set_value('last_name',$last_name); ?>">
                        <?php echo form_error('last_name'); ?>
                      </div>
                      </div>
                      </div>

                     <div class="row">
                     <div class="col-xs-6">
                       <div class="form-group">
                        <div class="infobox"></div>
                        <label for="inputname">Email</label>
                        <input class="form-control input" tabindex="4" id="email" name="email" type="email" placeholder="Customer Email" value="<?php echo set_value('email',$email); ?>">
                      </div>
                      </div>
                       <div class="col-xs-6">
                      <div class="form-group">
                        <div class="infobox"></div>
                        <label for="inputname">Phone</label>
                        <input class="form-control input" tabindex="5" id="phone" name="phone" type="tel" placeholder="Customer Phone Number" value="<?php echo set_value('phone',$phone); ?>">
                      </div>
                      </div>
                      <div class="row">
                      <div class="col-xs-6">
                        <div class="form-group">
                         <div class="col-xs-2">
                            <div class="radio"> 
                              <label>
                                <input type="radio" name="gender" id="gender" tabindex="6" value="male" <?php echo ($gender=='male' || $gender=='')? "checked":""?>>
                                Male
                              </label>
                            </div>
                          </div>
                          <div class="col-xs-2">
                            <div class="radio">
                              <label>
                                <input type="radio" name="gender" id="gender" tabindex="7" value="female" <?php echo ($gender=='female')? "checked":""?>>
                               Female
                              </label>
                            </div>
                          </div>
                         <div style="clear:both"></div>
                        </div>
                      </div>
                      <div class="col-xs-6">
                      <div class="form-group">
                        <label>Address</label>
                        <textarea class="form-control" rows="3" name="address" placeholder="Customer Address"><?php echo set_value('address',$address); ?></textarea>
                      </div>
                      </div>
                  </fieldset>
                  <button type="submit" tabindex="3" class="btn btn-block btn-lg btn-primary btn-flat" id="submit" name="submitcustomer" >Submit</button>
                  </div>
                  </form>
                  </div>
              </div>
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->