  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Inventory
        <small>List</small>
      </h1>
     <!--  <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <div class="col-xs-12">
        <?php echo $this->session->flashdata('msg');?>
         <div class="box">
          <div class="box-body table-responsive">
          <?php 
            echo $pagelist;
          ?>
          <div class="box-tools pull-right"><?php echo $pagination;?></div>
          </div>
          </div>
        </div>
    </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->