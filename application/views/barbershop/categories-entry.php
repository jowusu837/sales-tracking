<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Menu Category
            <?php $page_mode = ($this->uri->segment(2) != 'edit') ? 'new' : 'edit'; ?>
            <small><?php echo ucwords($page_mode); ?></small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Category Entry</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="post">
                        <?php echo $this->session->flashdata('msg'); ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label for="inputname" class="control-label">Name</label>
                                <input type="text" class="form-control" name="name" value="<?php echo set_value('name', $name); ?>" id="inputname" tabindex="1" placeholder="Category name" required>
                                <?php echo form_error('name'); ?>
                            </div>

                            <div class="form-group">
                                <label for="inputDescription" class="control-label">Description</label>
                                <textarea id="inputDescription" class="form-control" name="description" placeholder="Category description" tabindex="2"><?php echo set_value('description', $description); ?></textarea>
                                <?php echo form_error('description'); ?>
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-flat btn-primary pull-right">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->