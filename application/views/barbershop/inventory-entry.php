<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Inventory
            <?php
            if ($this->uri->segment(2) == 'add') {
                $page_mode = 'Re-Stock';
            }
            if ($this->uri->segment(2) == 'addUsed') {
                $page_mode = 'Record Used Item';
            }
            if ($this->uri->segment(2) == 'edit') {
                $page_mode = 'Edit';
            }
            if ($this->uri->segment(2) == 'editUsed') {
                $page_mode = 'Edit Used Item';
            }
            ?>
            <small>
                <?php echo ucwords($page_mode); ?>
            </small>
        </h1>
        <!-- <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
          <li class="active">Dashboard</li>
        </ol> -->
    </section>
    <!-- Main content -->
    <section class="content">
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">
                            Inventory Item Entry
                        </h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form method="post" role="form">
                        <?php
                        echo $this->
                        session->flashdata('msg');
                        ?>
                        <div class="box-body">
                            <div class="form-group">
                                <label class="control-label" for="inputname">
                                    Item
                                </label>
                                <?php
                                if ($readonly == true) {
                                    echo '<input name="service" type="hidden" value="' . $serviceSelected . '">';
                                    echo form_dropdown('service', $servicedata, $serviceSelected, array('class' => 'form-control input-md', 'id' => 'service', 'tabindex' => '1', 'disabled' => 'disabled'));
                                } else {
                                    echo form_dropdown('service', $servicedata, $serviceSelected, array('class' => 'form-control input-md', 'id' => 'service', 'tabindex' => '1'));
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="inputname">
                                    Quantity
                                </label>
                                <input class="form-control" id="inputname" name="quantity" placeholder="Item quantity" required="" tabindex="2" type="number" value="<?php echo set_value('quantity', $quantity); ?>">
                                <?php echo form_error('unit_price'); ?>    
                                </input>
                            </div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-flat btn-primary pull-right" type="submit">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>