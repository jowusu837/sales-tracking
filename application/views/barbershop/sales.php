<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            POINT OF SALE
            <small>NEW ENTRY</small>
        </h1>
        <!--  <ol class="breadcrumb">
           <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
           <li class="active">Dashboard</li>
         </ol> -->
    </section>

    <!-- Main content -->
    <section class="content" data-ng-app="app" data-ng-controller="AppController" data-ng-cloak>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
            <div class="col-xs-8">

                <?php echo $this->session->flashdata('msg'); ?>

                <form name="cartForm" novalidate autocomplete="off">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">ADD ITEM</h3>
                        </div>
                        <div class="box-body">
                            <div class="form-group form-group-lg">
                                <div class="input-group input-group-lg">
                                    <input type="text" class="form-control input-lg" placeholder="Search for..." ng-model="selectedItem" uib-typeahead="item.label for item in items | filter:$viewValue | limitTo:8" typeahead-on-select="selectItem($item, $model, $label, $event)">
                                    <span class="input-group-btn">
                                        <button class="btn btn-info" type="button"><i class="fa fa-cart-plus"></i> ADD TO CART</button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">CART</h3>
                        </div>
                        <div class="box-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="text-right">#</th>
                                        <th>ITEM</th>
                                        <th class="text-right">PRICE</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in cart track by $index">
                                        <td class="text-right">{{$index + 1}}</td>
                                        <td>{{item.label}}</td>
                                        <td class="text-right">{{item.price| number:2}}</td>
                                        <td class="text-right"><a href="javascript:;" data-ng-click="delteItemFromCart($index)"><i class="fa fa-minus-circle text-danger"></i></a></td>
                                    </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <p class="h1 text-success text-right">GHS {{totalPrice| number:2}}</p>
                        </div>
                    </div>
                    <div class="box" data-ng-show="cart.length">
                        <div class="box-header">
                            <h3 class="box-title">TENDER</h3>
                        </div>
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div data-ng-class="['form-group ', {'has-error': (cartForm.tenderAmount.$error.required || cartForm.tenderAmount.$error.min || cartForm.tenderAmount.$error.number)}]">
                                        <div class="input-group">
                                            <div class="input-group-addon">GHS</div>
                                            <input name="tenderAmount" type="number" class="form-control input-lg" placeholder="Amount paid" data-ng-model="tenderAmount" data-ng-change="getTenderChanage(tenderAmount)" min="{{totalPrice}}" data-ng-required="cartForm.$submitted" tabindex="1">
                                        </div>
                                        <span data-ng-show="cartForm.tenderAmount.$error.required" class="help-block" aria-hidden="true">Invalid amount!</span>
                                        <span data-ng-show="cartForm.tenderAmount.$error.min" class="help-block" aria-hidden="true">Amount cannot be less than GHS {{totalPrice | number:2}}</span>
                                        <span data-ng-show="cartForm.tenderAmount.$error.number" class="help-block" aria-hidden="true">Please enter figures only!</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <div class="input-group-addon">GHS</div>
                                        <input type="text" class="form-control input-lg" placeholder="Change" disabled style="background:#fff;" data-ng-model="tenderChange">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="checkoutButton" type="submit" class="btn btn-success btn-block btn-lg text-uppercase" style="margin-top: 40px;" data-ng-click="checkout()" data-ng-disabled="cartForm.$invalid" data-loading-text="PROCESSING CHECKOUT..."><i class="fa fa-money"></i> CHECKOUT</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!--Recent-->
            <div class="col-xs-4 ">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">RECENT SALES</h3>
                    </div>
                    <div class="box-body">
                        <?php echo $latestsaleslist; ?>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.row (main row) -->

        <div class="modal fade" tabindex="-1" role="dialog" id="amountModal">
            <form class="modal-dialog" name="itemAmountForm" novalidate autocomplete="off">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title text-uppercase">HOW MUCH?</h4>
                    </div>
                    <div class="modal-body">
                        <div data-ng-class="['form-group ', {'has-error': (itemAmountForm.amount.$error.required || itemAmountForm.amount.$error.min || itemAmountForm.amount.$error.number)}]">
                            <div class="input-group">
                                <div class="input-group-addon">GHS</div>
                                <input name="amount" type="number" class="form-control" data-ng-model="amount" min="{{selectedItem.service_price}}" data-ng-required="itemAmountForm.$submitted" tabindex="1" autofocus="true" >
                            </div>
                            <span data-ng-show="itemAmountForm.amount.$error.required" class="help-block" aria-hidden="true">Please specify amount!</span>
                            <span data-ng-show="itemAmountForm.amount.$error.min" class="help-block" aria-hidden="true">The minimum amount allowed for this item is GHS {{selectedItem.service_price}}</span>
                            <span data-ng-show="itemAmountForm.amount.$error.number" class="help-block" aria-hidden="true">Please enter figures only!</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">CANCEL</button>
                        <button type="submit" class="btn btn-primary" data-ng-click="addToCart(amount)" data-ng-disabled="itemAmountForm.$invalid">CONTINUE</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </form>
            <!-- /.modal-dialog -->
        </div>

    </section>
    <!-- /.content -->