<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['default_template'] = 'barbershop';
$config['base_url'] = 'http://localhost/sales-tracking';
$config['assets'] = 'assets';
$config['js_dir'] = $config['assets'].'/'.'js';
$config['css_dir'] = $config['assets'].'/'.'css';
$config['images'] = $config['assets'].'/'.'img';
$config['plugins'] = $config['assets'].'/'.'plugins';
$config['index_page'] = '';
$config['app_name'] = 'Mabwana';
$config['app_service_person'] = 'Barber';
$config['app_currency'] = 'GHS';
$config['app_company'] = 'Mabwana';
$config['app_company_phone'] = '+233 202 202 2002';
$config['payment_types'] = array('cash'=>'Cash','mobilemoney'=>'Mobile Money');
$config['sess_save_path'] = 'ci_sessions';
$config['sess_driver'] = 'database';