<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('listLatestSales')) {

    function listLatestSales($items) {
        $ci = & get_instance();
        $ci->load->model('Sales_model');
        $ci->load->model('Serviceperson_model');
        $output = '';
        $output.='<table class="table">' . "\n";
        $output.='<tr><th class="text-right">ID</th><th class="text-right">Amount (' . $ci->config->item('app_currency') . ')</th><th class="text-right">Time</th><th></th>' . "\n";
        $output.='<tbody>' . "\n";
        foreach ($items as $item) {
            $output.='<tr>
			<td class="text-right">' . $item['id'] . '</td>
			<td class="text-right"> ' . number_format($item['tender'] - $item['change_amount'], 2) . '</td>
			<td><span class="label label-success pull-right">' . gmdate('g:i A', strtotime($item['sales_at'])) . '</span></td>
                        <td class="text-right"><a href="#"><i class="fa fa-file-text"></i></a></td>
			</tr>' . "\n";
        }
        $output.='</tbody>' . "\n";
        $output.='</table>' . "\n";
        return $output;
    }

}

if (!function_exists('listSalesReportTable')) {

    function listSalesReportTable($items) {
        $ci = & get_instance();
        $ci->load->model('Serviceperson_model');
        $ci->load->model('Sales_model');
        $output = '';
        $output.='<table class="table">' . "\n";
        $output.='<tr><th>#</th><th>Service</th><th>Date</th><th>Time</th><th>Category</th><th> Customer</th><th> Sales By</th>
		<th>Tender</th><th>Amount</th>
		</tr>' . "\n";
        $output.='<tbody>' . "\n";
        $total_amount = 0;
        $paymenttypes = $ci->config->item('payment_types');
        foreach ($items as $item) {
            $salespersonInfo = $ci->Sales_model->Sales_by($item['ID']);
            $sales_by = ($salespersonInfo) ? ucwords($salespersonInfo['username']) : '';
            //$customerInfo=$ci->Sales_model->Sales_Customer($item['ID']);
            $customer_name = $item['first_name'] . ' ' . $item['last_name'];
            $output.='<tr>
			<td>' . $item['ID'] . '</td>
			<td>' . $item['service_name'] . '</td>
			
			<td>' . gmdate('j M, Y', strtotime($item['sales_at'])) . '</td>
			<td>' . gmdate(' g:i a', strtotime($item['sales_at'])) . '</td>
			<td>' . $item['category_name'] . '</td>
			<td>' . $customer_name . '</td>
			<td>' . $sales_by . '</td>
			<td>' . $item['tender'] . '</td>
			<td>' . $item['amount'] . '</td>
			</tr>' . "\n";
            $total_amount+=$item['amount'];
        }
        $output.='<tr><td colspan="100%" style="border-top:2px solid #000" colspan="100%"><div class="pull-right"><strong>Total Amount</strong><div> 
		<div class="pull-right">' . $ci->config->item('app_currency') . ' ' . $total_amount . '</div>
		</td></tr>' . "\n";
        $output.='</tbody>' . "\n";
        $output.='</table>' . "\n";
        return $output;
    }

}

if (!function_exists('print_receipt')) {

    function print_receipt($salesid, $items) {
        $ci = & get_instance();
        $ci->load->model('Sales_model');
        $customerInfo = $ci->Sales_model->Sales_Customer($salesid);
        $name = ($customerInfo) ? $customerInfo['first_name'] . ' ' . $customerInfo['last_name'] : '';
        $salespersonInfo = $ci->Sales_model->Sales_by($salesid);
        $sales_by = ($salespersonInfo) ? ucwords($salespersonInfo['username']) : '';
        $sales_info = $ci->Sales_model->getSalesInfo($salesid);
        $output = '';
        $output.='<table class="table" cellpadding="2" cellspacing="5">' . "\n";
        $output.='<tr ><th colspan="100%" style="border-bottom:1px solid #000">' . $ci->config->item('app_company') . '</th></tr>' . "\n";
        $output.='<tr><th align="left" colspan="100%">Receipt</th></tr>' . "\n";
        $output.='<tr><td align="left" colspan="100%">Phone: ' . $ci->config->item('app_company_phone') . '</td></tr>' . "\n";
        $output.='<tr><td align="left" style="border-bottom:1px solid #000" colspan="100%">Staff: ' . $sales_by . '</td></tr>' . "\n";
        $output.='<tr><td align="left" colspan="100%">Dated printed: ' . gmdate('M j, Y') . '</td></tr>' . "\n";
        $output.='<tr><td align="left" style="border-bottom:1px solid #000" colspan="100%">Name: ' . $name . '</td></tr>' . "\n";
        $output.='<tr><th align="left" style="border-bottom:1px solid #000">Sr.</th>
		<th style="border-bottom:1px solid #000">Service</th>
		<th style="border-bottom:1px solid #000">Date</th>
		<th style="border-bottom:1px solid #000"> Time</th>
		<th style="border-bottom:1px solid #000">Amount</th>
		</tr>' . "\n";


        $output.='<tbody>' . "\n";
        $i = 1;
        $total = 0;
        foreach ($items as $item) {
            $output.='<tr>
			<td align="left">' . $i . '.</td>
			<td>' . $item['service_name'] . '</td>
			<td>' . gmdate('j-m-Y', strtotime($item['sales_at'])) . '</td>
			<td>' . gmdate(' g:i a', strtotime($item['sales_at'])) . '</td>
			<td>' . $item['amount'] . '</td>
			</tr>' . "\n";
            $total+=$item['amount'];
            $i++;
        }
        $output.='<tr><td style="border-bottom:2px solid #000" colspan="100%"></td></tr>';
        $output.='<tr><td><strong>Total Bill:</strong></td><td></td><td></td><td></td><td>' . $ci->config->item('app_currency') . ' ' . $total . '</td></tr>';
        $output.='<tr><td><strong>Total Tender:</strong></td><td></td><td></td><td></td><td>' . $ci->config->item('app_currency') . ' ' . $sales_info['tender'] . '</td></tr>';
        $output.='<tr><td><strong>Change Due:</strong></td><td></td><td></td><td></td><td>' . $ci->config->item('app_currency') . ' ' . $sales_info['change_amount'] . '</td></tr>';
        $output.='</tr><td colspan="100%" align="center">Thank you for allowing us to serve you.</tr>' . "\n";
        $output.='</tbody>' . "\n";
        $output.='</table>' . "\n";
        return $output;
    }

}