<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('listTable'))
{
	function listTable($items)
	{
		$output='';
		$output.='<table class="table table-striped">'."\n";
		$output.='<tbody>'."\n";
		$output.='<tr><th>Name</th><th>Unit Price</th><th>Category</th><th></th></tr>'."\n";
		foreach ($items as $item)
		{
			$output.='<tr><td><a href="'.base_url('services/edit/'.$item['id']).'">'.$item['service_name'] . '</a>';
                        if($item['not_for_sale']){
                            $output .= '<span class="label label-warning pull-right">Not For Sale</span>';
                        }
                        if($item['inventory_part']){
                            $output .= '<span class="label label-info pull-right">Inventory Item</span>';
                        }
                        $output .= '</td><td>'.$item['service_price'].'</td><td>'.$item['category_name'].'</td><td><a href="'.base_url('services/edit/'.$item['id']).'" class="btn btn-sm btn-default btn-flat" title="Edit"><i class="fa fa-edit"></i></a>  <a href="'.base_url('services/delete/'.$item['id']).'" class="btn btn-sm btn-danger btn-flat" title="Delete"><i class="fa fa-trash"></i></a></td></tr>'."\n";
		}
		$output.='</tbody>'."\n";
		$output.='</table>'."\n";
		return $output;
	}
}

if(!function_exists('servicePersonlistTable'))
{
	function servicePersonlistTable($items)
	{
		$output='';
		$output.='<table class="table table-striped">'."\n";
		$output.='<tbody>'."\n";
		$output.='<tr><th>Name</th><th>Phone</th><th>Started Work</th><th>Status</th><th></th></tr>'."\n";
		foreach ($items as $item)
		{
			$started_work=($item['started_work']!='0000-00-00')? gmdate('F j, Y',strtotime($item['started_work'])):''; 
			$output.='<tr><td><a href="'.base_url('service-person/edit/'.$item['id']).'">'.$item['name'].'</a></td><td>'.$item['phone'].'</td><td>'.$started_work.'</td><td>'.ucwords($item['status']).'</td><td><a href="'.base_url('service-person/edit/'.$item['id']).'" class="btn btn-default btn-flat" title="Edit"><i class="fa fa-edit"></i></a>  <a href="'.base_url('service-person/delete/'.$item['id']).'" class="btn  btn-danger btn-flat" title="Delete"><i class="fa fa-trash"></i></a></td></tr>'."\n";
		}
		$output.='</tbody>'."\n";
		$output.='</table>'."\n";
		return $output;
	}
}