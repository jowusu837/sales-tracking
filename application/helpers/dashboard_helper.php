<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('numCustomers'))
{
	function numCustomers()
	{
		$ci =& get_instance();
		$ci->load->model('Customer_model');
		$total=$ci->Customer_model->totalNumCustomers();
		$output='';
		if($total)
		{
			$output='<div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>'.$total.'</h3>
              <p>Customers</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="'.base_url('customers').'" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>';
		}
		return $output;
	}
}

if(!function_exists('numSales'))
{
  function numSales()
  {
    $ci =& get_instance();
    $ci->load->model('Sales_model');
    $total=$ci->Sales_model->totalSales();
    $output='';
    if($total)
    {
      $output='<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>'.$total.'<sup style="font-size: 20px">'.$ci->config->item('app_currency').'</sup></h3>

              <p>Total Sales</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="'.base_url('sales-report').'" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>';
    }
    return $output;
  }
}

if(!function_exists('numServices'))
{
  function numServices()
  {
    $ci =& get_instance();
    $ci->load->model('Services_model');
    $total=$ci->Services_model->totalNumServices();
    $output='';
    if($total)
    {
      $output='<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>'.$total.'</h3>

              <p>Service</p>
            </div>
            <div class="icon">
              <i class="fa fa-shield"></i>
            </div>
            <a href="'.base_url('services').'" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>';
    }
    return $output;
  }
}

if(!function_exists('numServicePersons'))
{
  function numServicePersons()
  {
    $ci =& get_instance();
    $ci->load->model('Serviceperson_model');
    $total=$ci->Serviceperson_model->totalNumServicePersons();
    $output='';
    if($total)
    {
      $output.='<div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>'.$total.'</h3>

              <p>'.$ci->config->item('app_service_person').'s</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
            <a href="'.base_url('service-person').'" class="small-box-footer">
              More info <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>';
    }
    return $output;
  }
}