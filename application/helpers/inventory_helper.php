<?php

defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('listTable')) {

    function listTable($items) {
        $output = '';
        $output.='<table class="table">' . "\n";
        $output.='<tr><th >Item</th><th>Unit Price</th><th>Category</th><th title="Quantity in stock">Qty in stock</th></tr>' . "\n";
        $output.='<tbody>' . "\n";
        foreach ($items as $item) {
            $output.='<tr><td>' . $item['item_name'];
            if ($item['not_for_sale']) {
                $output .= '<span class="label label-warning pull-right">Not for Sale</span>';
            }
            $output .='</td><td>' . $item['unit_price'] . '</td><td>';
            if ($item['category_name']) {
                $output .= $item['category_name'];
            } else {
                $output .= 'Uncategorized';
            }
            $output .= '</td><td>' . (int) $item['qty_in_stock'] . '</td></tr>' . "\n";
        }
        $output.='</tbody>' . "\n";
        $output.='</table>' . "\n";
        return $output;
    }

}
