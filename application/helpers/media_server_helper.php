<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if(!function_exists('design_images')){
    function design_images($file='',$output=FALSE)
    {
        $ci = &get_instance();
        $file_path=base_url($ci->config->item('images').'/'.$file);;
        if($output)
        {
            return $file_path;
        }
        else
        {
            echo $file_path;
        }
        
    }
}
if(!function_exists('design_css')){
    function design_css($file='',$output=FALSE)
    {
        $ci = &get_instance();
        $file_path=base_url($ci->config->item('css_dir').'/'.$file);;
        if($output)
        {
            return $file_path;
        }
        else
        {
            echo $file_path;
        }
        
    }
}

if(!function_exists('design_plugins')){
    function design_plugins($file='',$output=FALSE)
    {
        $ci = &get_instance();
        $file_path=base_url($ci->config->item('plugins').'/'.$file);;
        if($output)
        {
            return $file_path;
        }
        else
        {
            echo $file_path;
        }
        
    }
}
if(!function_exists('design_js')){
    function design_js($file='',$output=FALSE)
    {
        $ci = &get_instance();
        $file_path=base_url($ci->config->item('js_dir').'/'.$file);;
        if($output)
        {
            return $file_path;
        }
        else
        {
            echo $file_path;
        }
        
    }
}

if(!function_exists('design_asset')){
    function design_asset($file='',$type='',$output=FALSE)
    {
        $ci = &get_instance();
        $file_path=base_url($ci->config->item($type.'_dir').'/'.$file);;
        if($output)
        {
            return $file_path;
        }
        else
        {
            echo $file_path;
        }
        
    }
}

/* End of file media_server_helper.php */
/* Location: ./system/helpers/media_server_helper.php */