<?php

/**
 * Create stock table
 * 
 * @property CI_DB_forge $dbforge 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_stock_table extends CI_Migration {

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'BIGINT',
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'item_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE,
            ),
            'quantity' => array(
                'type' => 'INT',
                'null' => FALSE,
                'unsigned' => TRUE
            )
        ));
        
        $this->dbforge->add_field("created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP");

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('stock');
    }

    public function down() {
        $this->dbforge->drop_table('stock');
    }

}
