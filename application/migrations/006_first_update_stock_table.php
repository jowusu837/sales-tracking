<?php

/**
 * Add user_id to stock table for tracking who enters what
 * 
 * @property CI_DB_forge $dbforge 
 */
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_First_update_stock_table extends CI_Migration {

    public function up() {
        $this->dbforge->add_column('stock', array(
            'user_id' => array(
                'type' => 'INT',
                'unsigned' => TRUE
            )
        ));
    }

    public function down() {
        $this->dbforge->drop_column('stock', 'user_id');
    }

}
